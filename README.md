CSV to XML
============

_The intent of these scripts is to streamline the population of repeating data in InDesign files._


This is a simple collection of scripts that can take a single csv or a list of csv files and convert them to xml.

step 1
--------
run `pip install requirements.txt`

step 2
-------
In main.py or mainAll.py add the CSV file to be converted to the csvFile variable or csvFiles list.

step 3
-------
run `python main.py` or python `mainAll.py`

step 4
-------
Profit.