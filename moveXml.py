
import os
import shutil

path = "csv"  # get folder as a command line argument
os.chdir(path)

xmlFiles = [x for x in os.listdir('.') if x.endswith('.xml') or x.endswith('.XML')]

for xml in xmlFiles:

    shutil.move(xml, '../xml')
